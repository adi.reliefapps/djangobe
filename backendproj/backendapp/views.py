from django.shortcuts import render
from rest_framework import viewsets
from .models import Hero, Group
from .serializers import HeroSerializer, GroupSerializer
# Create your views here.


class HeroView(viewsets.ModelViewSet):
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer


class GroupView(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
